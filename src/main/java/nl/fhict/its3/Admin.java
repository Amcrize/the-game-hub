package nl.fhict.its3;

public class Admin extends User {

    private String secretQuestionAnswer;

    public Admin(String firstName, String lastName, String username, String password, String secretQuestionAnswer) {
        super(firstName, lastName, username, password);
        this.secretQuestionAnswer = secretQuestionAnswer;
    }
}
